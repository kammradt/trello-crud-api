# Welcome :tophat:

Hi, the API link is [here](https://trello-crud-api.herokuapp.com/task)

## Things that need to be done:

- [ ] Create all tests
- [ ] Create all specific Exceptions
- [ ] Improve URLs / Endpoints
- [ ] Learn about deploy strategies -  CI;CD
- [ ] Use a better Database ex. PostgreSQL
- [ ] Create secure 'Environment Variables'

